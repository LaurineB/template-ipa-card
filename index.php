/**
* Upload app and post minimum infos needed to generateOTACard.
*
* MIT License
* Copyright (c) 2017 Laurine Baillet
*/
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Generer fiche OTA</title>
    <!-- style -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<!-- NavBar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="img/Lagardere-Active-blanc-noir.jpg" width="80" height="35"> </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)/">Accueil OTA <span class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projets <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)//ios">IOS</a></li>
                        <li><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)//android">Android</a></li>
                        <li><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)//blackberry">Blackberry</a></li>
                    </ul>
                </li>
                <li><a href="index.php">Générer fiche OTA</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="sheet col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

    <div class="col-sm-12">
        <h2>Générer une fiche OTA</h2>
        <h3>Etape 1 : Uploader le fichier</h3>
        <form enctype="multipart/form-data" action="" method="POST" >
            <div class="form-group">
                <input type="hidden" name="MAX_FILE_SIZE" value="4000000000" />
                <label for="file">Uploader ipa/apk</label>
                <input type="file" class="form-control" id="file" name="app">
            </div>
            <div class="form-group">
                <input type="submit" value="Envoyer">
            </div>
        </form>
        <?php
        $resultUpload = null;
        ////////////////////////
        // if form submitted //
        ////////////////////////
        if(isset($_FILES['app']) AND $_FILES['app']['error'] == 0) {
            $resultUpload = uploadFile();
            ?>
            <script>
                $('#upload').show();
            </script>
            <p id="upload">Upload en cours...</p>
            <p id="uploaded">Upload terminé.</p>
            <?php
        }
        ?>

        <h3>Etape 2 : Envoyer les informations</h3>
        <form action="generateOTACard.php" method="post">
            <div class="form-group">
                <label for="platform">Plateforme</label>
                <select class="form-control" id="platform" name="platform">
                    <option value="ios">IOS</option>
                    <option value="android">Android</option>
                </select>
            </div>
            <div id="iosForm">
                <div class="form-group">
                    <label for="distribution">Distribution</label>
                    <select class="form-control" id="distribution" name="distributionIOS">
                        <option value="InHouse">InHouse</option>
                        <option value="AdHoc">AdHoc</option>
                        <option value="Release">Release</option>
                        <option value="Debug">Debug</option>
                        <option value="InHouse-iphoneos">InHouse-iphoneos</option>
                        <option value="AdHoc-iphoneos">AdHoc-iphoneos</option>
                        <option value="Release-iphoneos">Release-iphoneos</option>
                        <option value="AppStore-iphoneos">AppStore-iphoneos</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="urlStorage" id="urlStorageLabel">Url de stockage de l'application (manifest)</label>
                    <input type="text" class="form-control" name="urlStorage" id="urlStorage" placeholder="https://mobiles.lagardere-active.com/var/storage/m///ios/ClubCaraV2/InHouse-iphoneos/application.ipa">
                </div>
                <div class="form-group">
                    <label for="urlIcon" id="urlIconLabel">Url de l'icon (manifest)</label>
                    <input type="text" class="form-control" name="urlIcon" id="urlIcon" value="https://mobiles.lagardere-active.com/extension/mobile_services/design/standard/images/icon114.png">
                </div>

                <div class="form-group">
                    <label for="urlInstallIOS">Url d'installation </label>
                    <input type="text" class="form-control" name="urlInstallIOS" id="urlInstallIOS" placeholder="itms-services://?action=download-manifest&url=https://mobiles.lagardere-active.com/application-info/ota_plist/(path)//ios/doctiBebe/InHouse/application.ipa">
                </div>
            </div>
            <div id="androidForm">
                <div class="form-group">
                    <label for="distributionAndroid">Distribution</label>
                    <input type="text" name="distributionAndroid" id="distributionAndroid" class="form-control">

                </div>
                <div class="form-group">
                    <label for="urlInstallAndroid">Url d'installation </label>
                    <input type="text" class="form-control" name="urlInstallAndroid" id="urlInstallAndroid" placeholder="http://mobilesla.ladmedia.fr/var/storage/m//android/tele7jeux/MotsFlechesT7J.apk">
                </div>
            </div>
            <div class="form-group">
                <?php
                //if upload succeed, show step 2 form submit button and post the path of the file uploaded
                if($resultUpload != null) {
                    ?>
                    <input type="submit" value="Générer .zip">
                    <input type="hidden" value="<?=$resultUpload?>" name="pathFile">
                    <?php
                }
                ?>
            </div>

        </form>
        <script type="text/javascript">
            //switch between different form depending of platform
            $('#androidForm').hide();
            $('#platform').change(function(){
                if($('#platform').val() == 'ios'){
                    $('#iosForm').show();
                    $('#androidForm').hide();
                } else {
                    $('#androidForm').show();
                    $('#iosForm').hide();
                }
            });
        </script>
    </div>
</div>
</body>
</html>
<?php
/**
 * @return null|string
 *
 * upload the app and save it in /uploads/
 */
function uploadFile() {

    $destination = "/uploads/";
    if(isset($_FILES['app']) AND $_FILES['app']['error'] == 0) {
        $fileMoved = move_uploaded_file($_FILES['app']['tmp_name'],  __DIR__.$destination. basename($_FILES['app']['name']));
        if($fileMoved) {
            ?>
            <script>
                $('#upload').hide();
                $('#uploaded').show();
                $('#step2').show();
            </script>
            <?php
            return __DIR__.$destination. basename($_FILES['app']['name']);
        } else {
            ?>
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Erreur!</strong> Le transfert à échoué.
                <script>
                    $('#upload').hide();
                    $('#uploaded').hide();
                </script>
            </div>
            <?php
            return null;
        }
    }
}

?>