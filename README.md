# Template-ipa-card

Application for generate an html app card and a manifest usable in OTA. 

## Use

Complete form in index.php in 2 steps.

First : upload the .ipa and succeed

Second : complete the form with minimum infos to create the manifest.

You will be redirect to a download page.

## License 

License MIT Copyright (c) 2017 Laurine Baillet
