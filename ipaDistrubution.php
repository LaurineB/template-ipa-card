<?
/**
 * Creates an Manifest from any IPA iPhone application file for iOS Wireless App Distribution.
 * and searches for the right provision profile in the same folder
 *
 * Script needs GD Library (for resizing the images) and CFPropertyList class (http://code.google.com/p/cfpropertylist/)

 * @author Wouter van den Broek <wvb@webstate.nl>
 */

class ipaDistrubution {

    /**
     * The base url of the script.
     */
    protected $baseurl;
    /**
     * The base folder of the script.
     */
    protected $basedir;
    /**
     * The temp folder of the script.
     */
    protected $tempDir;
    /**
     * The folder of the app where the manifest will be written.
     */
    public $folder;
    /**
     * iTunesArtwork name which is an standard from Apple (http://developer.apple.com/iphone/library/qa/qa2010/qa1686.html).
     */
    protected $itunesartwork = "iTunesArtwork";
    /**
     * App name which can be used for the HTML page.
     */
    public $appname;
    /**
     * Bundle version which can be used for the HTML page.
     */
    public $bundleversion;
    /**
     * Bundle version marketing
     */
    public $shortBundleVersion;
    /**
     * Mobileprovision
     */
    public $mobileprovision;
    /**
     * App icon which can be used for the HTML page.
     */
    public $appicon = "AppIcon83.5x83.5@2x~ipad.png";
    /**
     * The link to the manifest for the iPhone .
     */
    public $applink = "itms-services://?action=download-manifest&url=";
    /**
     * Bundle identifier which is used to find the proper provision profile.
     */
    public $identifier;
    /**
     * Bundle icon name for extracting icon file
     */
    protected $icon;
    /**
     * The name of the provision profile for the IPA iPhone application .
     */
    public $provisionprofile;

    /**
     * url icon of LAD
     */
    protected $displayIcon = "https://mobiles.lagardere-active.com/extension/mobile_services/design/standard/images/icon114.png";
    /**
     * Distribution type (adHoc,Release,Debug,InHouse)
     */
    public $distribution;

    /**
     * path of zip file generated
     */
    public $pathZip;

    /**
     * Path to ipa
     */
    private $ipa;

    /**
     * Initialize the IPA and create the Manifest.
     *
     * @param String $ipa the IPA file for which an Manifest must be made
     */
    public function __construct($ipa,$destination,$urlStorage,$urlIcon){
        $this->baseurl = "http".((!empty($_SERVER['HTTPS'])) ? "s" : "")."://".$_SERVER['SERVER_NAME'];
        $this->basedir = (strpos($_SERVER['REQUEST_URI'],".php")===false?$_SERVER['REQUEST_URI']:dirname($_SERVER['REQUEST_URI'])."/");
        $this->tempDir = $this->baseurl.$this->basedir.'temp';

        $this->ipa = $ipa;

        //Remove querystring from basedir
        if( strpos( $this->basedir, "?" ) > 0 ) {
            $questionmark_position = strpos( $this->basedir, "?" );
            $this->basedir = substr( $this->basedir, 0, $questionmark_position);
        }
        $this->makeDir($destination);

        $this->getPlist();
        $this->getAppIcon();
        $this->createManifest($urlStorage,$urlIcon);

        $this->seekMobileProvision($this->identifier);

        $this->getIcon();

        $this->getMobileProvision();

        if (file_exists($this->itunesartwork)) {
            $this->makeImages();
        }

    }

    /**
     * Make a folder where the Manifest and icon files are held.
     *
     * @param String $dirname name of the folder
     */
    function makeDir ($dirname) {
        $this->folder = $dirname;
        if (!is_dir($dirname)) {
            if (!mkdir($dirname)) die('Failed to create folder '.$dirname.'... Is the current folder writeable?');
        }
    }

    /**
     * Get de Plist and iTunesArtwork from the IPA file
     *
     */
    function getPlist () {
        if (is_dir($this->folder)) {
            $zip = zip_open($this->ipa);
            if ($zip) {
                while ($zip_entry = zip_read($zip)) {
                    $fileinfo = pathinfo(zip_entry_name($zip_entry));
                    if ($fileinfo['basename']=="Info.plist"||$fileinfo['basename']==$this->itunesartwork) {
                        $fp = fopen($fileinfo['basename'], "w");
                        if (zip_entry_open($zip, $zip_entry, "r")) {
                            $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                            fwrite($fp,"$buf");
                            zip_entry_close($zip_entry);
                            fclose($fp);
                        }
                    }
                }
                zip_close($zip);
            }
        }
    }
    /**
     * get the app icon
     */
    function getAppIcon() {
        if (is_dir($this->folder)) {
            $zip = zip_open($this->ipa);
            if ($zip) {
                while ($zip_entry = zip_read($zip)) {
                    $fileinfo = pathinfo(zip_entry_name($zip_entry));
                    if ($fileinfo['basename']==$this->appicon||$fileinfo['basename']==$this->itunesartwork) {
                        $fp = fopen('temp/OTACard/'."appIcon.png", "w");
                        if (zip_entry_open($zip, $zip_entry, "r")) {
                            $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                            fwrite($fp,"$buf");
                            zip_entry_close($zip_entry);
                            fclose($fp);
                        }
                    }
                }
                zip_close($zip);
            }
        }
    }
    /**
     * Create the icon (if not in IPA) and itunes artwork from the original iTunesArtwork
     */
    function makeImages () {
        if (function_exists("ImageCreateFromJPEG")) {
            $im = @ImageCreateFromJPEG ($this->itunesartwork);
            $x = @getimagesize($this->itunesartwork);
            $iTunesfile = @ImageCreateTrueColor (512, 512);
            @ImageCopyResampled ($iTunesfile, $im, 0, 0, 0, 0, 512, 512, $x[0], $x[1]);
            @ImagePNG($iTunesfile,$this->folder."/itunes.png",0);
            @ImageDestroy($iTunesfile);
            if ($this->icon==null) {
                $iconfile = @ImageCreateTrueColor (57, 57);
                @ImageCopyResampled ($iconfile, $im, 0, 0, 0, 0, 57, 57, $x[0], $x[1]);
                @ImagePNG($iconfile,$this->folder."/icon.png",0);
                @ImageDestroy($iconfile);
                $this->appicon = $this->folder."/icon.png";
            }
        }
    }


    /**
     * Get the icon file out of the IPA and place it in the right folder
     */
    function getIcon () {
        if (is_dir($this->folder)) {
            $zip = zip_open($this->ipa);
            if ($zip) {
                while ($zip_entry = zip_read($zip)) {
                    $fileinfo = pathinfo(zip_entry_name($zip_entry));
                    if ($fileinfo['basename']==$this->icon) {
                        $fp = fopen($this->folder.'/'.$fileinfo['basename'], "w");
                        if (zip_entry_open($zip, $zip_entry, "r")) {
                            $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                            fwrite($fp,"$buf");
                            zip_entry_close($zip_entry);
                            fclose($fp);
                        }
                        $this->appicon = $this->folder."/".$this->icon;
                    }
                }
                zip_close($zip);
            }
        }
    }

    /**
     * Get the .mobileprovision file out of the IPA and place it in the right folder
     */
    function getMobileProvision () {
        if (is_dir($this->folder)) {
            $zip = zip_open($this->ipa);
            if ($zip) {
                while ($zip_entry = zip_read($zip)) {
                    $fileinfo = pathinfo(zip_entry_name($zip_entry));
                    if ($fileinfo['basename']=="embedded.mobileprovision") {
                        $fp = fopen($this->folder.'/'.$fileinfo['basename'], "w");
                        if (zip_entry_open($zip, $zip_entry, "r")) {
                            $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                            fwrite($fp,"$buf");
                            zip_entry_close($zip_entry);
                            fclose($fp);
                        }
                        $this->mobileprovision = $fileinfo['basename'];
                    }
                }
                zip_close($zip);
            }
        }
    }

    /**
     * @param $urlStorage
     * @param $urlIcon
     *
     * Parse the Plist and get the values for the creating an Manifest and write the Manifest
     *
     */
    function createManifest ($urlStorage,$urlIcon) {
        if (file_exists(dirname(__FILE__).'/cfpropertylist/CFPropertyList.php')) {
            require_once(dirname(__FILE__).'/cfpropertylist/CFPropertyList.php');

            $plist = new CFPropertyList('Info.plist');
            $plistArray = $plist->toArray();
            //var_dump($plistArray);
            $this->identifier = $plistArray['CFBundleIdentifier'];
            $this->appname = $plistArray['CFBundleName'];
            $this->bundleversion = $plistArray['CFBundleVersion'];
            $this->shortBundleVersion = $plistArray['CFBundleShortVersionString'];

            $manifest = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
			<dict>
				<key>items</key>
				<array>
					<dict>
						<key>assets</key>
						<array>
							<dict>
								<key>kind</key>
								<string>software-package</string>
								<key>url</key>
								<string>'.$urlStorage.'</string>
							</dict>
							<dict>
								<key>kind</key>
								<string>full-size-image</string>
								<key>needs-shine</key>
								<false/>
								<key>url</key>
								<string>'.$urlIcon.'</string>
							</dict>
							<dict>
								<key>kind</key>
								<string>display-image</string>
								<key>needs-shine</key>
								<false/>
								<key>url</key>
								<string>'.$urlIcon.'</string>
							</dict>
						</array>
						<key>metadata</key>
						<dict>
							<key>bundle-identifier</key>
							<string>'.$plistArray['CFBundleIdentifier'].'</string>
							<key>bundle-version</key>
							<string>'.$plistArray['CFBundleVersion'].'</string>
							<key>kind</key>
							<string>software</string>
							<key>title</key>
							<string>'.$plistArray['CFBundleIdentifier'].'</string>
						</dict>
					</dict>
				</array>
			</dict>
			</plist>';

            if (file_put_contents($this->folder."/manifest.plist",$manifest)) $this->applink = $this->applink.$this->baseurl.$this->basedir.$this->folder."/".basename($this->ipa, ".ipa").".plist";
            else die("Wireless manifest file could not be created !?! Is the folder ".$this->folder." writable?");


        } else die("CFPropertyList class was not found! You need it to create the wireless manifest. Put it in de folder cfpropertylist!");
    }

    /**
     * Search for the right provision profile in de current folder
     *
     * @param String $identifier the bundle identifier for the app
     */
    function seekMobileProvision ($identifier) {
        $wildcard = pathinfo($identifier);

        $bundles = array();
        foreach (glob("*.mobileprovision") as $filename) {
            $profile = file_get_contents($filename);
            $seek = strpos(strstr($profile, $wildcard['filename']),"</string>");
            if ($seek!== false) $bundles[substr(strstr($profile, $wildcard['filename']),0,$seek)] = $filename;
        }

        if (array_key_exists($this->identifier,$bundles)) $this->provisionprofile = $bundles[$this->identifier];
        else if  (array_key_exists($wildcard['filename'].".*",$bundles)) $this->provisionprofile = $bundles[$wildcard['filename'].".*"];
        else $this->provisionprofile = null;
    }

    /**
     * Make a zip with manifest, mobile provision and html.
     */
    function zipFile() {

        $zip = new ZipArchive();
        $filename = 'temp/OTACard.zip';
        $this->pathZip = $filename;
        $zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->folder),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $filename = $file->getBasename();

                // Add current file to archive
                $zip->addFile($filePath, $filename);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();
    }

    /**
     * Clear all temp files except zip file
     */
    function clear() {
        unlink($this->ipa);

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->folder),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            unlink($file);
        }
        unlink($this->folder);
    }

}
