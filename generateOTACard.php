/**
* Generate a set to present an app in OTA for IOS and Android.
*
*
* Script needs ApkParser by Xuxinhua & IPA-Distribution by Wouter van den Broek
*
* MIT License
* Copyright (c) 2017 Laurine Baillet
*/
<?php
include 'autoload.php';
require_once("ipaDistrubution.php");
require_once("ApkParser.php");

//list($platform,$urlStorage,$pathFile,$urlIcon,$distribution,$urlInstall) = ("");

if(isset($_POST['platform'])) {$platform = $_POST['platform'];}
if(isset($_POST['urlStorage'])) {$urlStorage = $_POST['urlStorage'];}
if(isset($_POST['pathFile'])) {$pathFile = $_POST['pathFile'];}
if(isset($_POST['urlIcon'])) {$urlIcon = $_POST['urlIcon'];}

if($platform == "ios") {
    if(isset($_POST['distributionIOS'])) {$distribution = $_POST['distributionIOS'];}
    if(isset($_POST['urlInstallIOS'])) {$urlInstall = $_POST['urlInstallIOS'];}
} else {
    if(isset($_POST['distributionAndroid'])) {$distribution = $_POST['distributionAndroid'];}
    if(isset($_POST['urlInstallAndroid'])) {$urlInstall = $_POST['urlInstallAndroid'];}
}

//Destination folder to transform in zip
$destination = "temp/OTACard";

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Upload</title>
    <!-- style -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
    <!-- NavBar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="img/Lagardere-Active-blanc-noir.jpg" width="80" height="35"> </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)/">Accueil OTA <span class="sr-only">(current)</span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projets <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)//ios">IOS</a></li>
                            <li><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)//android">Android</a></li>
                            <li><a href="http://mobiles.lagardere-active.com/application-info/ota/(path)//blackberry">Blackberry</a></li>
                        </ul>
                    </li>
                    <li><a href="index.php">Générer fiche OTA</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="sheet col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

        <div class="col-sm-12">
            <?php
            if ($platform == "ios") {

                if (pathinfo($pathFile)['extension'] == "ipa") {
                    $ipa = new ipaDistrubution($pathFile, $destination,$urlStorage,$urlIcon);
                    $provision_profile = $ipa->mobileprovision;
                    $app_name = $ipa->appname;
                    $bundle_version = $ipa->bundleversion;
                    $bundleId = $ipa->identifier;
                    $version = $ipa->shortBundleVersion;

                    $card = "<h2>".$app_name." ".$platform."</h2>
                        <div class=\"col-sm-12 col-md-3\">
                            <img src=\"appIcon.png\" class=\"appIcon\">
                        </div>
                        <div class=\"col-sm-12 col-md-8 col-md-offset-1\">
                            <table class=\"table-infos\">
                                <tr>
                                    <td><strong>Distribution</strong></td><td>".$distribution."</td>
                                </tr>
                                <tr>
                                    <td><strong>Bundle Identifier</strong></td>
                                    <td>".$bundleId."</td>
                                </tr>
                                <tr>
                                    <td><strong>Build version</strong></td>
                                    <td>".$bundle_version."</td>
                                </tr>
                                <tr>
                                    <td><strong>Version</strong></td>
                                    <td>".$version."</td>
                                </tr>
                            </table>
                            <div class=\"btn-group\">
                                <a href=\"".$urlInstall."\" class=\"btn btn-primary btn-large\">Install</a>
                                <button class=\"btn btn-primary btn-large dropdown-toggle\" data-toggle=\"dropdown\">
                                    <span class=\"caret\"></span>
                                </button>
                                <ul class=\"dropdown-menu\">
                                    <a href=\"".$provision_profile."\" style=\"margin: 10px;\">Mobileprovision</a>
                                </ul>
                            </div>
                        </div>";

                    generateHTML($card,$destination,$platform);

                } else {
                    ?>
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erreur!</strong> le fichier n'est pas un .ipa
                        <?php
                        unlink($destination . '/' . $_FILES['file']['name'])
                        ?>
                    </div>
                    <?php
                }
            } else if ($platform == "android") {
                if (pathinfo($pathFile)['extension'] == "apk") {
                    $apkParser = new ApkParser($pathFile);
                    $apkParser->open($pathFile);

                    $card = " <h2>".$apkParser->getAppName()." ".$platform."</h2>
                <div class=\"col-sm-12 col-md-3\">
                    <img src=\"\" class=\"appIcon\">
                </div>
                <div class=\"col-sm-12 col-md-8 col-md-offset-1\">
                    <table class=\"table-infos\">
                        <tr>
                            <td><strong>Distribution</strong></td><td>".$distribution."</td>
                        </tr>
                        <tr>
                            <td><strong>Bundle Identifier</strong></td>
                            <td>".$apkParser->getPackage()."</td>
                        </tr>
                        <tr>
                            <td><strong>Version name</strong></td>
                            <td>".$apkParser->getVersionName()."</td>
                        </tr>
                        <tr>
                            <td><strong>Version code</strong></td>
                            <td>".$apkParser->getVersionCode()."</td>
                        </tr>
                    </table>
                    <div class=\"btn-group\">
                        <a href=\"".$urlInstall."\" class=\"btn btn-primary btn-large\">Install</a>
                    </div>
                </div>";

                    generateHTML($card,$destination,$platform);

                } else {
                    ?>
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erreur!</strong> le fichier n'est pas un .apk
                        <?php
                        unlink($pathFile)
                        ?>
                    </div>
                    <?php
                }
            }

            if($platform == "ios") {
                $ipa->zipFile();
                ?>
                <h2>Votre zip est prêt !</h2>
                <a href="<?=$ipa->pathZip?>" class="btn btn-primary">Télécharger</a>
                <?php
                $ipa->clear();

            } else if($platform == "android") {
                ?>
                <h2>Votre fiche est prête !</h2>
                <a href="temp/OTACard.html" class="btn btn-primary" download="OTACard">Télécharger</a>
                <?php
                unlink($pathFile);
                //Clear temp files except .html downloadable
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator('temp/OTACard/'),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );

                foreach ($files as $name => $file)
                {
                    unlink($file);
                }
            }

            ?>

        </div>
    </div>
</body>
</html>

<?php
/**
 * @param $card
 * @param $destination
 * @param $platform
 *
 * Generate the card with infos of the app
 */
function generateHTML($card,$destination,$platform) {
    //prendre le nom de l'app avant
    $generateHtml = "OTACard.html";
    if($platform == "ios") {
        $fh = fopen($destination.'/'.$generateHtml, 'w');
    } else {
        $fh = fopen('temp/' . $generateHtml, 'w');
    }
    $header = "<!DOCTYPE html>
<html lang=\"fr\">
<head>
    <meta charset=\"UTF-8\">
    <title>OTA Card</title>
    <!-- style -->
    <link rel=\"stylesheet\" href=\"css/style.css\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
    <style>
html {
    margin: 0;
    padding: 0;
    height: 100%;
}

body {
    background-color: #e7e7e7 !important;
    height: 100%;
}

h1,h2,h3 {
    margin: 20px !important;
}

.sheet {
    background: white !important;
    margin-top: 100px;
    padding: 10px;
    padding-bottom: 30px;
    min-height: 500px;
    border-radius: 5px;
    box-shadow: rgba(70, 70, 70, 0.26) 1px 1px 1px 1px;
}
.appIcon {
    height: 83.5px;
    width : 83.5px;
    margin: 10px;
}
.table-infos td {
    padding: 5px;
}
.navbar {
    background-color: black !important;
}
</style>
</head>
<body>
<!-- NavBar -->
<nav class=\"navbar navbar-inverse navbar-fixed-top\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"#\">OTA</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">
                <li class=\"active\"><a href=\"http://mobiles.lagardere-active.com/application-info/ota/(path)/\">Accueil OTA <span class=\"sr-only\">(current)</span></a></li>
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Projets <span class=\"caret\"></span></a>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"http://mobiles.lagardere-active.com/application-info/ota/(path)//ios\">IOS</a></li>
                        <li><a href=\"http://mobiles.lagardere-active.com/application-info/ota/(path)//android\">Android</a></li>
                        <li><a href=\"http://mobiles.lagardere-active.com/application-info/ota/(path)//blackberry\">Blackberry</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class=\"sheet col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2\">

    <div class=\"col-sm-12\">";


    $footer ="    </div>
</div>
</body>
</html>";

    $otaCard = $header.$card.$footer;

    fwrite($fh,$otaCard);
    fclose($fh);
}

?>